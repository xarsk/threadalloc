#include <pthread.h>
#include <unistd.h>
#include "malloc.h"
#include <stdio.h>
#include <string.h>

#define STEP 32*1024

//freelist lock mutex initialisation
static pthread_mutex_t freelist_locks[16] = {
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER,
        PTHREAD_MUTEX_INITIALIZER
};

static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

//freelist initialisation
static memBlock *free_lists[16] = {NULL};

memBlock *create_block(int i, memBlock *new_block, int size_wanted) {
    new_block = sbrk(0);
    void *request = sbrk(size_wanted + sizeof(memBlock));
    new_block = request;
    if (request == (void *) -1) {
        printf("SBRK FAILED \n");
        return new_block;
    }
    new_block->s.size = size_wanted;
    new_block->s.is_free = 1;
    new_block->s.next = NULL;
    free_lists[i] = new_block;
    printf("Item placed in freelist no. %d.\n", i);
    return new_block;

    /*
     * Gets called by find_block() to create a new block in memory to use
     *
     * It requests from the OS the size asked, plus the size for our struct (metadata size)
     * If OS was able to give that memory, our new memory block gets the values needed,
     * places itself in the freelist and gets returned.
     *
     * */
}

memBlock *find_block(int i, int size_wanted, memBlock *new_block) {
    if (free_lists[i]) {
        new_block = free_lists[i];
        while (new_block) {
            printf("Something already exist in  freelist no. %d!\n", i);
            if (new_block->s.is_free && new_block->s.size >= size_wanted) {
                printf("Item moved to list no. %d.\n", i);
                return new_block;
            } else {
                printf("Found block in freelist no. %d but it's not usable. Searching next block...\n", i);
                new_block = new_block->s.next;
            }
        }
        printf("Unable to find unused or big enough blocks in freelist no. %d. Creating new block...\n", i);
        new_block = create_block(i, new_block, size_wanted);
    } else {
        printf("freelist no. %d is empty. Creating new block...\n", i);
        new_block = create_block(i, new_block, size_wanted);
    }
    return new_block;
    /*
     * Gets called by mymalloc() to assign a variable to a block.
     *
     * If freelist is empty (ex. first run), create_block() is called.
     * Otherwise, it checks if an existing block could be used (ex. after being freed)
     * and either uses that block, or if block is too small or not free, again create_block is called.
     *
     * */
}

void *mymalloc(size_t size) {

    memBlock *header;
    int i;

    if (!size)// Don't bother running if called to allocate 0 bytes
        return NULL;

    /*
     *
     * Finds the appropriate freelist to place item.
     * A reminder, step is 32KB and freelists start from 1 up to 16.
     * If size is more than 32KB*16, item is placed in the last freelist.
     * The mutex of the chosen freelist gets locked to avoid simultaneous usage of the same freelist,
     * in order to run our program in threads.
     *
     * find_block() gets called to place item to appropriate freelist.
     *
     * */

    for (i = 1; i <= 16; i++) {
        if ((size < STEP * i + 1) || (size > STEP * 16 && i == 16)) {
            printf("Item will go in freelist no. %d.\n", i);
            pthread_mutex_lock(&freelist_locks[i]);
            header = find_block(i, size, header);
            break;
        }
    }

    /*
     *
     * If header returned successfully, gets marked as used,
     * mutex gets released and block's data (not metadata) gets returned.
     *
     * */

    if (header) {
        header->s.is_free = 0;
        pthread_mutex_unlock(&freelist_locks[i]);
        return (void *) (header + 1);
    }

}

void *mycalloc(size_t num, size_t nsize) {
    size_t size;
    void *memBlock;
    if (!num || !nsize) {
        printf("CALLOC - Number or size null\n");
        return NULL;
    }
    size = num * nsize;
    if (nsize != size / num) {
        printf("CALLOC - size overflow!\n");
        return NULL;
    }
    memBlock = mymalloc(size);
    if (!memBlock) {
        printf("CALLOC - Malloc failed to allocate memory to block\n");
        return NULL;
    }
    memset(memBlock, 0, size);
    return memBlock;
}

void myfree(void *block) {
    memBlock *header;
    void *programbreak;

    if (!block) { //Don't bother running if given block is null.
        printf("FREE - block doesn't exist");
        return;
    }
    void *address = block; //Just for the printf message
    pthread_mutex_lock(&lock);
    header = (memBlock *) block - 1; //-1 in order to get block's header and not data
    programbreak = sbrk(0);

    if ((char *) block + header->s.size == programbreak) {//If block is at the end of program's break (heap)
        for (int i = 1; i <= 16; i++) {
            if ((header->s.size <= STEP * i + 1) || (header->s.size > STEP * i && i == 16)) {
                if (free_lists[i] == NULL) {
                    free_lists[i] = header;
                } else {
                    while (free_lists[i]->s.next != NULL)
                        free_lists[i] = free_lists[i]->s.next;
                    free_lists[i]->s.next = header;
                    header->s.is_free = 1;
                    printf("%p released.\n", address);
                }
                sbrk(0 - sizeof(memBlock) - header->s.size);//Memory gets released at the end of the run.
                printf("Program break reduced. Memory freed.\n");
                pthread_mutex_unlock(&lock);
                return;
            }
        }
    } else { //If block is somewhere in memory (not at the end) just set it free
        header->s.is_free = 1;
        printf("%p released.\n", address);
        pthread_mutex_unlock(&lock);
        return;
    }
}
