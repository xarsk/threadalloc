#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "threadpool.h"
#include "malloc.h"

#define NUM_THREADS 5
#define NUM_ITEMS 100

void worker(void *arg) {
    int *val = arg;
    int old = *val;
    srand(time(NULL));
    *val +=1000;
    printf("tid=%ld, old=%d, val=%d\n", pthread_self(), old, *val);

    if (*val % 2)
        usleep(100000);
}

int main(int argc, char **argv) {
    int choice;

    printf("Type 1 for random malloc test or 2 for thread-pool test: \n");
    scanf("%d", &choice);

    if (choice == 1) {
        int r[NUM_ITEMS];
        char *str[NUM_ITEMS];

        for (int i = 0; i < NUM_ITEMS; i++) {//Calling mymalloc()
            r[i] = rand() % 1024 * 32 * 17; //Create random value from 0 up to more than 512KB to test mymalloc()
            str[i] = mymalloc(r[i]);
            printf("No. %d: Size %dKB, should be in freelist no. %d/16, address %p\n\n", i, r[i] / 1024,
                   r[i] / 1024 / 32 + 1,
                   str[i]);
        }

        for (int i = 0; i < NUM_ITEMS; i++) {//Calling myfree()
            printf("No. %d with address \t", i);
            myfree(str[i]);
        }
        return 0;

    } else if (choice == 2) {
        tpool_t *work_queue;
        int *vals;
        size_t i;

        work_queue = tpool_create(NUM_THREADS); //Initialize threadpool

        vals = mycalloc(NUM_ITEMS, sizeof(*vals)); //Allocate memory for each number

        for (i = 0; i < NUM_ITEMS; i++) {
            vals[i] = i;
            tpool_add_work(work_queue, worker, vals + i); //Add work in queue
            printf("No. %d - Adding work to worker\n", i);
        }
        printf("  Calling wait...\n");
        tpool_wait(work_queue);
        for (i = 0; i < NUM_ITEMS; i++) {
            printf("Result of no. %d, %d\n", i, vals[i]);
        }
        printf("  Calling free...\n");
        myfree(vals);
        printf("  Calling tp destroy...\n");
        tpool_destroy(work_queue); //Destroy threadpool
        return 0;
    }
}
