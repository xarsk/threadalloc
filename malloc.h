#ifndef MYOTHERMALLOC_MALLOC_H
#define MYOTHERMALLOC_MALLOC_H

#include <stddef.h>

typedef char ALIGN[16];

union header {
    struct {
        size_t size;
        unsigned is_free;
        union header *next;
    } s;
    ALIGN stub;
};
typedef union header memBlock;

void *mymalloc(size_t size);
void *mycalloc(size_t n, size_t size);
void myfree(void *block);

#endif //MYOTHERMALLOC_MALLOC_H
