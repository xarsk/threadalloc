//
// Created by linuxvm on 13/9/19.
//

#ifndef MYOTHERMALLOC_THREADPOOL_H
#define MYOTHERMALLOC_THREADPOOL_H

#include <stdbool.h>
#include <stddef.h>

struct tpool;
typedef struct tpool tpool_t;

typedef void (*thread_func_t)(void *arg);

tpool_t *tpool_create(size_t num);
void tpool_destroy(tpool_t *work_queue);

bool tpool_add_work(tpool_t *work_queue, thread_func_t func, void *arg);
void tpool_wait(tpool_t *work_queue);
#endif //MYOTHERMALLOC_THREADPOOL_H
