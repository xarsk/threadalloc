#include <stdio.h>
#include <pthread.h>
#include "threadpool.h"
#include "malloc.h"


// work struct
typedef struct tpool_work {
    thread_func_t      func;
    void              *arg;
    struct tpool_work *next;
} tpool_work_t;


// work queue
struct tpool {
    tpool_work_t    *work_first; // first work item
    tpool_work_t    *work_last; // last work item
    pthread_mutex_t  work_mutex; // locking mutex
    pthread_cond_t   work_cond; // work_cond signals the threads when there's work to be done
    pthread_cond_t   working_cond; // working_cond signals when no threads are processing
    size_t           working_thread_cnt; // working threads counter
    size_t           alive_thread_cnt; // alive threads counter
    bool             stop; // stop threads
};


// create work
static tpool_work_t *tpool_work_create(thread_func_t func, void *arg){
    tpool_work_t *work;

    if (func == NULL)
        return NULL;

    work       = mymalloc(sizeof(*work)); // allocate memory for work item
    work->func = func;
    work->arg  = arg;
    work->next = NULL;
    return work;
}


// destroy work
static void tpool_work_destroy(tpool_work_t *work){
    if (work == NULL)
        return;
    myfree(work);
}


// get work from queue
static tpool_work_t *tpool_work_get(tpool_t *work_queue){
    tpool_work_t *work;

    if (work_queue == NULL)
        return NULL;

    work = work_queue->work_first;
    if (work == NULL)
        return NULL;

    if (work->next == NULL) {
        work_queue->work_first = NULL;
        work_queue->work_last  = NULL;
    } else {
        work_queue->work_first = work->next;
    }
    return work;
}


static void *tpool_worker(void *arg){
    tpool_t      *work_queue = arg;
    tpool_work_t *work;

    while (1) {
        // lock mutex
        pthread_mutex_lock(&(work_queue->work_mutex));

        // check if work_queue has requested that all threads stop running and exit
        if (work_queue->stop)
            break;

        // check if there is work in queue
        if (work_queue->work_first == NULL)
            pthread_cond_wait(&(work_queue->work_cond), &(work_queue->work_mutex));

        work = tpool_work_get(work_queue); // get work from queue
        work_queue->working_thread_cnt++; // increase working threads counter
        pthread_mutex_unlock(&(work_queue->work_mutex));


        // process work object and destroy it when finished
        if (work != NULL) {
            work->func(work->arg);
            tpool_work_destroy(work);
        }

        pthread_mutex_lock(&(work_queue->work_mutex));
        work_queue->working_thread_cnt--; // reduce working threads counter


        if (!work_queue->stop && work_queue->working_thread_cnt == 0 && work_queue->work_first == NULL)
            pthread_cond_signal(&(work_queue->working_cond));

        pthread_mutex_unlock(&(work_queue->work_mutex));
    }

    work_queue->alive_thread_cnt--;
    pthread_cond_signal(&(work_queue->working_cond));
    pthread_mutex_unlock(&(work_queue->work_mutex));
    return NULL;
}


// work_queue creation
tpool_t *tpool_create(size_t num){
    tpool_t   *work_queue;
    pthread_t  thread;
    size_t     i;

    work_queue = mycalloc(1, sizeof(*work_queue));
    work_queue->alive_thread_cnt = num;

    pthread_mutex_init(&(work_queue->work_mutex), NULL);
    pthread_cond_init(&(work_queue->work_cond), NULL);
    pthread_cond_init(&(work_queue->working_cond), NULL);

    work_queue->work_first = NULL;
    work_queue->work_last  = NULL;

    for (i=0; i<num; i++) {
        pthread_create(&thread, NULL, tpool_worker, work_queue);
        pthread_detach(thread);
    }

    return work_queue;
}


// work_queue destruction
void tpool_destroy(tpool_t *work_queue){
    tpool_work_t *work;
    tpool_work_t *work2;

    if (work_queue == NULL){
        printf("TPOOL DESTROY - work_queue null\n");
        return;
    }

    pthread_mutex_lock(&(work_queue->work_mutex));
    work = work_queue->work_first;

    while (work != NULL) {
        work2 = work->next;
        tpool_work_destroy(work);
        work = work2;
    }

    work_queue->stop = true;
    pthread_cond_broadcast(&(work_queue->work_cond));
    pthread_mutex_unlock(&(work_queue->work_mutex));

    tpool_wait(work_queue);

    pthread_mutex_destroy(&(work_queue->work_mutex));
    pthread_cond_destroy(&(work_queue->work_cond));
    pthread_cond_destroy(&(work_queue->working_cond));

    printf("  Calling free for work_queue...\n");
    myfree(work_queue);
    printf("Program finished successfully.\n");

}


// add work in queue
bool tpool_add_work(tpool_t *work_queue, thread_func_t func, void *arg){
    tpool_work_t *work;

    if (work_queue == NULL){
        printf("TRDPL ADD WORK - work_queue null (work_queue)\n");
        return false;
    }

    work = tpool_work_create(func, arg);

    if (work == NULL){
        printf("TRDPL ADD WORK - Work null\n");
        return false;
    }

    pthread_mutex_lock(&(work_queue->work_mutex));

    if (work_queue->work_first == NULL) {
        work_queue->work_first = work;
        work_queue->work_last  = work_queue->work_first;
    } else {
        work_queue->work_last->next = work;
        work_queue->work_last       = work;
    }

    pthread_cond_broadcast(&(work_queue->work_cond));
    pthread_mutex_unlock(&(work_queue->work_mutex));
    return true;
}

void tpool_wait(tpool_t *work_queue){
    if (work_queue == NULL)
        return;

    pthread_mutex_lock(&(work_queue->work_mutex));
    while (1) {
        if ((!work_queue->stop && work_queue->working_thread_cnt != 0) || (work_queue->stop && work_queue->alive_thread_cnt != 0)) {
            pthread_cond_wait(&(work_queue->working_cond), &(work_queue->work_mutex));
        } else {
            break;
        }
    }
    pthread_mutex_unlock(&(work_queue->work_mutex));
}
